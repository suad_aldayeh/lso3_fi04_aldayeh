import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;

public class Form_andern extends JFrame {

	private JPanel contentPane;
	private JLabel Aufgabe_Label_1;
	private JButton btnRot;
	private JButton btnGrün;
	private JButton btnBlau;
	private JButton btnGelb;
	private JButton btnStandardFarbe;
	private JButton btnFarbeWählen;
	private JLabel Aufgabe_Label_2;
	private JButton btnArial;
	private JButton btnCourierNew;
	private JTextField txt_Eingaben;
	private JButton btnTextImLabelLöschen;
	private JButton btnblueSchriftFarbe_2;
	private JButton btnSchwarzSchriftFarbe_1;
	private JLabel Aufgabe_Label_4;
	private JButton btnPlus;
	private JButton btnMinus;
	private JLabel Aufgabe_Label_5;
	private JButton btnLinksbündig;
	private JButton btnZentriert;
	private JButton btnRechtsbündig;
	private JLabel Aufgabe_Label_6;
	private JButton btnExit;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_andern frame = new Form_andern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Form_andern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 416, 668);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 240, 240));

		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel Lblxxxx = new JLabel("Diese Text soll ver\u00E4ndert werden");
		Lblxxxx.setHorizontalAlignment(SwingConstants.CENTER);
		Lblxxxx.setFont(new Font("Arial", Font.BOLD, 12));
		Lblxxxx.setBounds(110, 32, 204, 14);
		contentPane.add(Lblxxxx);
		
		Aufgabe_Label_1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		Aufgabe_Label_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		Aufgabe_Label_1.setForeground(new Color(0, 0, 0));
		Aufgabe_Label_1.setLabelFor(this);
		Aufgabe_Label_1.setBounds(10, 61, 207, 14);
		contentPane.add(Aufgabe_Label_1);
		
		btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.red);
					}
		});
		

		btnRot.setToolTipText("");
		btnRot.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnRot.setBounds(10, 104, 117, 23);
		contentPane.add(btnRot);
		
		btnGrün = new JButton("Gr\u00FCn");
		btnGrün.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.green);

			}
		});
		btnGrün.setToolTipText("");
		btnGrün.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnGrün.setBounds(276, 104, 118, 23);
		contentPane.add(btnGrün);
		
		btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.blue);

			}
		});
		
		btnBlau.setToolTipText("");
		btnBlau.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnBlau.setBounds(148, 104, 118, 23);
		contentPane.add(btnBlau);
		
		btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.yellow);

			}
		});
		btnGelb.setToolTipText("");
		btnGelb.setFont(new Font("Arial", btnGelb.getFont().getStyle() | Font.BOLD, btnGelb.getFont().getSize() + 1));
		btnGelb.setBounds(10, 148, 117, 23);
		contentPane.add(btnGelb);
		
		btnStandardFarbe = new JButton("Standardfarbe");
		btnStandardFarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(new Color (0xEEEEEE));

			}
		});
		
		btnStandardFarbe.setToolTipText("");
		btnStandardFarbe.setBounds(148, 148, 118, 23);
		contentPane.add(btnStandardFarbe);
		
		btnFarbeWählen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWählen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			}
		});
		btnFarbeWählen.setToolTipText("");
		btnFarbeWählen.setForeground(Color.BLACK);
		btnFarbeWählen.setBackground(Color.LIGHT_GRAY);
		btnFarbeWählen.setBounds(209, 148, 116, 23);
		
		Aufgabe_Label_2 = new JLabel("Aufgabe 2: Text Formatieren");
		Aufgabe_Label_2.setForeground(Color.BLACK);
		Aufgabe_Label_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		Aufgabe_Label_2.setBounds(10, 192, 207, 14);
		contentPane.add(Aufgabe_Label_2);
		
		btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int groesse = Lblxxxx.getFont().getSize();
				Lblxxxx.setFont(new Font("Arial", Font.PLAIN, groesse + 0));
			}
		});
		btnArial.setToolTipText("");
		btnArial.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnArial.setBounds(10, 216, 117, 23);
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int groesse = Lblxxxx.getFont().getSize();
				Lblxxxx.setFont(new Font("Comic Sans MS", Font.PLAIN, groesse + 0));
			}
		});
		btnComicSansMs.setToolTipText("");
		btnComicSansMs.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnComicSansMs.setBounds(148, 216, 118, 23);
		contentPane.add(btnComicSansMs);
		
		btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int groesse = Lblxxxx.getFont().getSize();
				Lblxxxx.setFont(new Font("Courier New", Font.PLAIN, groesse + 0));
			}
		});
		btnCourierNew.setToolTipText("");
		btnCourierNew.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnCourierNew.setBounds(276, 216, 118, 23);
		contentPane.add(btnCourierNew);
		
		txt_Eingaben = new JTextField();
		txt_Eingaben.setText("Hier bitte Text eingeben");
		txt_Eingaben.setBounds(10, 269, 364, 20);
		contentPane.add(txt_Eingaben);
		txt_Eingaben.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Lblxxxx.setText(txt_Eingaben.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(10, 312, 164, 23);
		contentPane.add(btnInsLabelSchreiben);
		
		btnTextImLabelLöschen = new JButton("Text im Label l\u00F6schen");
		btnTextImLabelLöschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Lblxxxx.setText("");
			}
		});
		btnTextImLabelLöschen.setBounds(209, 312, 164, 23);
		contentPane.add(btnTextImLabelLöschen);
		
		JLabel Aufgabe_Label_3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		Aufgabe_Label_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		Aufgabe_Label_3.setBounds(20, 346, 217, 14);
		contentPane.add(Aufgabe_Label_3);
		
		JButton btnRotSchriftFarbe_2 = new JButton("Rot");
		btnRotSchriftFarbe_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Lblxxxx.setForeground(Color.RED);

			}
		});
		btnRotSchriftFarbe_2.setToolTipText("");
		btnRotSchriftFarbe_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnRotSchriftFarbe_2.setBounds(10, 370, 117, 23);
		contentPane.add(btnRotSchriftFarbe_2);
		
		btnblueSchriftFarbe_2 = new JButton("Blau");
		btnblueSchriftFarbe_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Lblxxxx.setForeground(Color.blue);

			}
		});
		btnblueSchriftFarbe_2.setToolTipText("");
		btnblueSchriftFarbe_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnblueSchriftFarbe_2.setBounds(148, 371, 118, 23);
		contentPane.add(btnblueSchriftFarbe_2);
		
		btnSchwarzSchriftFarbe_1 = new JButton("Schwarz");
		btnSchwarzSchriftFarbe_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Lblxxxx.setForeground(Color.black);

			}
		});
		btnSchwarzSchriftFarbe_1.setToolTipText("");
		btnSchwarzSchriftFarbe_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSchwarzSchriftFarbe_1.setBounds(276, 370, 118, 23);
		contentPane.add(btnSchwarzSchriftFarbe_1);
		
		Aufgabe_Label_4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		Aufgabe_Label_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		Aufgabe_Label_4.setBounds(10, 406, 217, 14);
		contentPane.add(Aufgabe_Label_4);
		
		btnPlus = new JButton("+");
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int plus = Lblxxxx.getFont().getSize();
				Lblxxxx.setFont(new Font("Arial", Font.PLAIN, plus + 1));
			}
		});
		btnPlus.setBounds(10, 449, 164, 23);
		contentPane.add(btnPlus);
		
		btnMinus = new JButton("-");
		btnMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int plus = Lblxxxx.getFont().getSize();
				Lblxxxx.setFont(new Font("Arial", Font.PLAIN, plus - 1));
			}
		});
		btnMinus.setBounds(209, 449, 165, 23);
		contentPane.add(btnMinus);
		
		Aufgabe_Label_5 = new JLabel("Aufgabe 5: Textausrichtung");
		Aufgabe_Label_5.setFont(new Font("Tahoma", Font.BOLD, 11));
		Aufgabe_Label_5.setBounds(10, 489, 217, 14);
		contentPane.add(Aufgabe_Label_5);
		
		btnLinksbündig = new JButton("linksb\u00FCndig");
		btnLinksbündig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Lblxxxx.setBounds(0, 32, 204, 14 );
			}
		});
		btnLinksbündig.setBounds(10, 514, 117, 23);
		contentPane.add(btnLinksbündig);
		
		btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Lblxxxx.setBounds(110, 32, 204, 14 );
			}
		});
		btnZentriert.setBounds(148, 514, 118, 23);
		contentPane.add(btnZentriert);
		
		btnRechtsbündig = new JButton("rechtsb\u00FCndig");
		btnRechtsbündig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Lblxxxx.setBounds(200, 32, 204, 14 );

			}
		});
		btnRechtsbündig.setBounds(276, 514, 118, 23);
		contentPane.add(btnRechtsbündig);
		
		Aufgabe_Label_6 = new JLabel("Aufgabe 6: Programm beenden");
		Aufgabe_Label_6.setFont(new Font("Tahoma", Font.BOLD, 11));
		Aufgabe_Label_6.setBounds(10, 561, 217, 14);
		contentPane.add(Aufgabe_Label_6);
		
		btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);
			}
			
			
		});
		btnExit.setBounds(10, 576, 380, 53);
		contentPane.add(btnExit);
		
		btnNewButton = new JButton("Farbe w\u00E4hlen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(JColorChooser.showDialog(contentPane, "Wähle Farbe", Color.WHITE));
			}
		});
		btnNewButton.setBounds(276, 148, 118, 23);
		contentPane.add(btnNewButton);
	}
}
